width=32;
height=50;

layer_height=0.3;
extrusion_width=0.9;
wall_extrusions=1;
base_layers=6;
base_overhang=2;

base_width=width-2*base_overhang;
base_height=base_layers*layer_height;

seed_starter();

translate([0, 0, base_height + 0.1])
    rotate([180, 0, 0])
        seed_starter_base();

module seed_starter()
{
    inset_offset=2*wall_extrusions*extrusion_width;
    translate([0, 0, height/2])
    {
        difference()
        {
            cube([width, width, height], center=true);

            translate([0, 0, base_height/2])
            {
                cube([width-inset_offset, width-inset_offset, height], center=true);
            }

            translate([0, 0, -height/2 + base_height/4])
            {
                cube([base_width-base_overhang, base_width-base_overhang, base_height], center=true);
            }
        }
    }
}

module seed_starter_base()
{
    inset_offset=2*base_overhang;

    difference()
    {
        cube([base_width, base_width, base_height], center=true);

        translate([0, 0, base_height/2])
        {
            difference()
            {
                cube([base_width, base_width, base_height], center=true);
                cube([base_width-inset_offset, base_width-inset_offset, base_height], center=true);
            }
        }

        cylinder(d=6, h=2*base_height, center=true, $fn=16);

        for(i=[0:3])
        {
            rotate([0, 0, i*90])
            {
                translate([base_width/3, 0, 0])
                {
                    cylinder(d=3, h=2*base_height, center=true, $fn=16);
                }
            }
        }
    }
}